'use strict'

require('dotenv').config()

// Require external modules.
const Fastify = require('fastify')

// Require internal modules.
const config = require('./config')

// Instantiate the fastify application.
const app = Fastify(config.fastify)

// Register the application as a normal plugin.
app.register(require('./app'))

// Listen on the specified port.
app.listen(config.PORT, (err, address) => {
  if (err) throw err
  console.log(`App listening on ${config.PORT}.`)
})
