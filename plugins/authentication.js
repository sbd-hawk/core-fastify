'use strict'

const bcrypt = require('bcryptjs')
const fp = require('fastify-plugin')

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    saltRounds: 10,
    namespace: 'authentication'
  }, opts)

  // Create the cargo.
  const cargo = {
    /**
     * Hash a plaintext value.
     * @param {string} password Value to hash.
     * @return {string} The hash of the input.
     */
    hashPassword: async  (password) => {
      return await bcrypt.hash(password, opts.saltRounds)
    },

    /**
     * Validate a plaintext value against a hash.
     * @param {string} password Plaintext to validate.
     * @param {string} hash Hash to validate.
     * @return {boolean} Whether the inputs match.
     */
    verifyPassword: async (password, hash) => {
      return await bcrypt.compare(password, hash)
    }
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn('Using the `fastify-authentication` plugin without a namespace is not recommended and may result in name collisions.')
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'fastify-authentication'
})
