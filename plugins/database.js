'use strict'

const fp = require('fastify-plugin')

/**
 * Prepare the database collections.
 * @param {object} db The database instance.
 * @param {object} schemas All schemas.
 */
async function primeDatabase (db, schemas) {
  for (let schema in schemas) {
    // Get the information for he mongodb schema.
    const mongoSchema = schemas[schema].mongoSchema

    // Create the collection.
    const collection = await db.createCollection(mongoSchema.name)

    // Add the validator.
    await db.command({
      collMod: collection.collectionName,
      validator: mongoSchema.validator
    })

    // Create the indexes.
    for (let j = 0; j < mongoSchema.indexes.length; j++) {
      await collection.createIndex(mongoSchema.indexes[j].keys, mongoSchema.indexes[j].options)
    }
  }
}

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    mongodbURI: fastify.config.MONGODB_URI,
    schemas: require('../schemas')
  }, opts)

  // Register the fastify-mongodb plugin.
  fastify.register(require('fastify-mongodb'), {
    url: opts.mongodbURI,
    forceClose: true,
    useNewUrlParser: true
  })

  // When ready, prime the database.
  fastify.ready(function () {
    primeDatabase(fastify.mongo.db, opts.schemas)
  })
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'hawk-database',
  dependencies: [ 'fastify-env' ]
})
