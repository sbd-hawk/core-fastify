'use strict'

const fp = require('fastify-plugin')

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    jwtSecret: fastify.config.JWT_SECRET,
    namespace: 'authorization'
  }, opts)

  // Register the fastify-jwt plugin.
  fastify.register(require('fastify-jwt'), {
    secret: opts.jwtSecret
  })

  // Create the cargo.
  const cargo = {
    /**
     * Pre handler function to check if a JWT is provided and verify it.
     * @param {function} extraAuth Function to run extra checks.
     * @param {object} extraAuth.req Fastify request.
     * @param {object} extraAuth.claims Raw claims from the decoded JWT.
     * @return {function} Function to be run pre handler.
     */
    requiresJWT: (extraAuth = async (req, claims) => true) => {
      // Return the auth function.
      return async function (req, reply) {
        // Verify the incoming JWT.
        const claims = await fastify.jwt.verify(req.cookies.token)

        // Check if the session is blacklisted.
        let invalid = await fastify.daos.sessions.isBlacklisted(fastify.convert.toObjectID(claims.jti))
        if (invalid) {
          reply
            .code(401)
            .type('application/json')
            .send({ message: 'session has ended' })
          return
        }

        // Run the extra verification function.
        if (!await extraAuth(req, claims)) {
          reply
            .code(401)
            .type('application/json')
            .send({ message: 'access not granted for this user' })
        }
      }
    },

    /**
     * Common extra authorization checks.
     */
    checks: {
      /**
       * Extra authorization function for checking if the user is an admin.
       * To be passed into requiresJWT.
       * @param {object} req Fastify request.
       * @param {object} claims Raw claims from the decoded JWT.
       * @return {boolean} If the user is an admin.
       */
      adminOnly: async function (req, claims) {
        const user = await fastify.daos.users.getByID(fastify.convert.toObjectID(claims.sub))
        return user.role === 'admin'
      }
    }
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn('Using the `fastify-authorization` plugin without a namespace is not recommended and may result in name collisions.')
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'fastify-authorization',
  dependencies: [ 'fastify-env', 'fastify-convert', 'hawk-daos' ]
})
