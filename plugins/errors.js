'use strict'

const fp = require('fastify-plugin')

class APIError extends Error {
  constructor (message, code) {
    super(message)
    this.code = code
  }
}

async function plugin (fastify, opts) {
  opts = Object.assign({
    namespace: 'errors'
  }, opts)

  const cargo = {
    DOC_DOES_NOT_EXIST: new APIError('document does not exist', 60000),
    BAD_CREDENTIALS: new APIError('incorrect credentials provided', 60001)
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn('Using the `hawk-errors` plugin without a namespace is not recommended and may result in name collisions.')
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'hawk-errors',
  dependencies: []
})
