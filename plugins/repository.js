'use strict'

const fp = require('fastify-plugin')
const { repository, pagination } = require('../schemas/abstract')

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    namespace: 'repository'
  }, opts)

  const cargo = {
    schema: (schema) => {
      // Configure the specific repository.
      const specRepository = Object.assign({
        repositorySchema: schema.schema,
        additionalProperties: false
      }, repository)

      // Create the response.
      const response = {
        type: 'array',
        items: {
          type: 'object',
          properties: schema.outputProperties
        }
      }

      // Return the entire repository schema.
      return {
        querystring: specRepository,
        response: {
          200: {
            type: 'object',
            properties: {
              data: response,
              pagination
            }
          }
        }
      }
    },

    handler: (collectionName) => {
      return async function (req, reply) {
        // Set the collection.
        const coll = this.mongo.db.collection(collectionName)

        // Get the total number of documents.
        const total = await coll.countDocuments(req.query.filters)

        // Set the pagiantion options.
        const pages = Math.ceil(total / req.query.perPage)
        const page = req.query.page < 1 ? 1 : req.query.page > pages && pages ? pages : req.query.page

        // Get the paginated repository.
        const repo = await coll
          .find(req.query.filters)
          .sort(req.query.sort)
          .skip((page - 1) * req.query.perPage)
          .limit(req.query.perPage)
          .toArray()

        // Return data.
        reply
          .code(200)
          .type('application/json')
          .send({
            data: repo,
            pagination: { page, pages, total, perPage: req.query.perPage }
          })
      }
    }
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn('Using the `fastify-repository` plugin without a namespace is not recommended and may result in name collisions.')
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'fastify-repository',
  dependencies: [ 'fastify-mongodb' ]
})
