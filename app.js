'use strict'

const fp = require('fastify-plugin')

const config = require('./config')

// Set the version.
const _VERSION = [1, 0, 0]
const VERSION = _VERSION.join('.')

// Export as a plugin.
module.exports = async function (fastify, opts) {
  // Register eveything with fastify instance. Order matters.
  fastify
    .setSchemaCompiler(schema => require('./utils/validation').compile(schema))
    .register(require('fastify-swagger'), config.swaggerOpts)
    .register(require('fastify-cookie'))
    .register(require('fastify-env'), { schema: config.envSchema })
    .register(require('fastify-helmet'))
    .register(require('fastify-sensible'))
    .register(require('./plugins/database'))
    .register(require('./daos'))
    .register(require('./plugins/convert'))
    .register(require('./plugins/authentication'))
    .register(require('./plugins/authorization'))
    .register(require('./plugins/errors'))
    .register(require('./plugins/repository'))
    .register(require('./services'))
    .decorate('version', VERSION)
}
