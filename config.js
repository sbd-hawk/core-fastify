'use strict'

const os = require('os')

const swaggerOpts = {
  routePrefix: '/docs',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'Hawk Core',
      description: 'Hawk Core provides a REST API for the core functionality of the Hawk application.',
      version: '1.0.0'
    },
    host: 'app.sbd-hawk.com',
    schemes: [ 'https' ],
    consumes: [ 'application/json' ],
    produces: [ 'application/json' ]
  }
}

const envSchema = {
  type: 'object',
  required: [ 'JWT_SECRET', 'MONGODB_URI', 'REDIS_URI' ],
  properties: {
    PORT: { type: 'integer', default: 5000 },
    BASE_PATH: { type: 'string', default: '' },
    LOGGER: { type: 'boolean', default: true },
    VERSION_IN_PATH: { type: 'boolean', default: false },
    JWT_SECRET: { type: 'string' },
    MONGODB_URI: { type: 'string' },
    REDIS_URI: { type: 'string' },
    NUM_WORKERS: { type: 'integer', default: os.cpus().length }
  }
}

const fastify = {
  ignoreTrailingSlash: true,
  bodyLimit: 16777216,
  disableRequestLogging: true,
  caseSensative: false,
  trustProxy: true
}

module.exports = { swaggerOpts, envSchema, fastify }
