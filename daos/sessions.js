'use strict'

const { SESSION } = require('../schemas')
const GenericDAO = require('./generic')
const ERRORS = require('../utils/errors')

class SessionsDAO extends GenericDAO {
  /**
   * Create an instance.
   * @param {object} db The database instance.
   */
  constructor (db) {
    super(db, SESSION)
  }

  /**
   * Get all sessions for a specific user.
   * @param {ObjectID} userID User id to fetch sessions for.
   * @return {array} Array containing all sessions for a user.
   */
  async getByUser (userID) {
    return await this.collection.find({ userID }).toArray()
  }

  /**
   * Check if a user is in session.
   * @param {ObjectID} userID User id to fetch sessions for.
   * @return {boolean} If the user is in session.
   */
  async isInSession (userID) {
    return await this.collection.countDocuments({ userID }) > 0
  }

  /**
   * End the session.
   * @param {ObjectID} _id The id of the session to end.
   */
  async end (_id) {
    // Update the session document.
    const { matchedCount } = await this.collection.updateOne(
      { _id },
      { $set: { endAt: new Date() } }
    )

    // Check if an update occured, throw error if not.
    if (matchedCount !== 1) throw ERRORS.DOC_DOES_NOT_EXIST
  }

  /**
   * Check if a session is blacklisted.
   * @param {ObjectID} _id The id of the session to check.
   * @return {boolean} If the session has been ended.
   */
  async isBlacklisted (_id) {
    // Get the session.
    const session = await this.getByID(_id)

    return session.endAt !== null
  }
}

module.exports = SessionsDAO
