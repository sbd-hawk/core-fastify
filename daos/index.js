'use strict'

const fp = require('fastify-plugin')

const DAOs = {
  'users': require('./users'),
  'sessions': require('./sessions')
}

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    namespace: 'daos'
  }, opts)

  // Create the cargo by instantiating each DAO.
  const cargo = {}
  for (let dao in DAOs) {
    cargo[dao] = new DAOs[dao](fastify.mongo.db)
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn('Using the `hawk-daos` plugin without a namespace is not recommended and may result in name collisions.')
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'hawk-daos',
  dependencies: [ 'hawk-database' ]
})
