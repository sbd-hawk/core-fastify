'use strict'

const bcrypt = require('bcryptjs')

const { USER } = require('../schemas')
const DAO = require('./generic')
const ERRORS = require('../utils/errors')

class UsersDAO extends DAO {
  /**
   * Create an instance.
   * @param {object} db The database instance.
   */
  constructor (db) {
    super(db, USER)
  }

  /**
   * Get a specific user by the username.
   * @param {string} username The username to fetch.
   * @return {object} The requested user's document.
   */
  async getByUsername (username) {
    // Fetch the user document.
    const doc = await this.collection.findOne({ username })

    // Check if a user document was returned, throw error if not.
    if (doc === null) throw ERRORS.DOC_DOES_NOT_EXIST

    return doc
  }

  /**
   * Authenticate a user.
   * @param {string} username The username to authenticate.
   * @param {string} password The password to authenticate.
   * @return {boolean} If the username and password match.
   */
  async authenticate (username, password) {
    // Fetch the user document.
    const doc = await this.collection.findOne(
      { username },
      { projection: { password: 1, authentication: 1 } }
    )

    // Check if a user document was returned, throw error if not.
    if (doc === null) throw ERRORS.DOC_DOES_NOT_EXIST

    // Check the hash against the given password.
    if (!await bcrypt.compare(password, doc.password)) throw ERRORS.BAD_CREDENTIALS

    return { userID: doc._id, auth: doc.authentication }
  }

  /**
   * Update a user's information.
   * @param {ObjectID} _id The user id to update.
   * @param {object} update The fields to update.
   * @return {object} The updated user document.
   */
  async update (_id, update) {
    // Get the current time.
    update.updateAt = new Date()

    // If a password is given update the time.
    if ('password' in update) {
      update.lastPasswordUpdate = new Date()
    }

    // Update the specified user.
    const { matchedCount } = await this.collection.updateOne(
      { _id },
      { $set: update }
    )

    // Check if an update occured, throw error if not.
    if (matchedCount !== 1) throw ERRORS.DOC_DOES_NOT_EXIST

    // Get the new user document.
    const user = await this.collection.findOne({ _id })

    return user
  }

  /**
   * Delete a user by setting them as inactive.
   * @param {ObjectID} _id The user id to delete.
   * @return {boolean} The time the operations occured at.
   */
  async deactivate (_id) {
    // Get the current time.
    const deleteAt = new Date()

    // Update the specified user.
    const { matchedCount } = await this.collection.updateOne({ _id }, { $set: { deleteAt } })

    // Check if an update occured, throw error if not.
    if (matchedCount !== 1) throw ERRORS.DOC_DOES_NOT_EXIST

    return deleteAt
  }

  /**
   * Check if a user is active.
   * @param {ObjectID} _id The user id to check.
   * @return {boolean} If the user is active.
   */
  async isActive (_id) {
    const result = await this.collection.findOne({ _id }, { projection: { deleteAt: 1 } })
    return result.deleteAt === null
  }
}

module.exports = UsersDAO
