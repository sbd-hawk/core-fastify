'use strict'

// Eventually abstract this.
const DOC_DOES_NOT_EXIST = new Error('document does not exist')
DOC_DOES_NOT_EXIST.code = 10061

class DAO {
  /**
   * Create an instance.
   * @param {object} db The database instance.
   * @param {Schema} schema The schema of the collection.
   */
  constructor (db, schema) {
    this.collection = db.collection(schema.name)
    this.schema = schema
  }

  /**
   * Get all documents.
   * @param {number} skip The number of documents to skip.
   * @param {number} limit The number of documents to retrieve.
   * @return {array} All documents.
   */
  async getAll (skip, limit) {
    return await this.collection.find({}, { skip, limit })
  }

  /**
   * Get the count of all documents.
   * @return {number} Count of documents.
   */
  async getAllCount () {
    return await this.collection.countDocuments()
  }

  /**
   * Get a specific document by the _id.
   * @param {ObjectID} _id The document id to fetch.
   * @return {object} The requested document.
   */
  async getByID (_id) {
    // Fetch the document.
    const doc = await this.collection.findOne({ _id })

    // Check if a document was returned, throw error if not.
    if (doc === null) throw DOC_DOES_NOT_EXIST

    return doc
  }

  /**
   * Create a new document.
   * @param {object} init The initial document values, added after the default values.
   * @return {object} The created document.
   */
  async create (init) {
    // Get the default document model and add the initial data.
    const doc = Object.assign({}, this.schema.defaultModel, init)

    // Insert the document.
    const result = await this.collection.insertOne(doc)

    return result.ops[0]
  }

  /**
   * Purge a document by removing it from the collection.
   * @param {ObjectID} _id The document id to purge.
   */
  async purge (_id) {
    return await this.collection.remove({ _id }, { justOne: true })
  }
}

module.exports = DAO
