'use strict'

const BSON = require('bson')

const { PROCESS, Schema } = require('./schema')
const { isTrue } = require('../utils/convert')

/**
 * Collection Name
 * Name of the collection in MongoDB.
 * @type {string}
 */
const collectionName = 'users'

/**
 * Schema
 * JSON Schema used to validate documents in the database.
 * @type {object}
 */
const schema = {
  bsonType: 'object',
  required: [ '_id', 'createAt', 'updateAt', 'deleteAt', 'username', 'password', 'failedAttempts', 'lastPasswordUpdate', 'authentication', 'authorization' ],
  additionalProperties: false,
  properties: {
    _id: { bsonType: 'objectId' },
    createAt: { bsonType: 'date' },
    updateAt: { bsonType: 'date' },
    deleteAt: { bsonType: [ 'null', 'date' ] },
    username: { bsonType: 'string' },
    password: { bsonType: 'string' },
    failedAttempts: { bsonType: 'int' },
    lastPasswordUpdate: { bsonType: 'date' },
    authentication: {
      bsonType: 'object',
      required: [ 'accessTokenDuration', 'blacklistable' ],
      additionalProperties: false,
      properties: {
        accessTokenDuration: { bsonType: 'long' },
        blacklistable: { bsonType: 'bool' }
      }
    },
    authorization: {
      bsonType: 'object',
      required: [ 'modules', 'sites', 'grants' ],
      additionalProperties: false,
      properties: {
        modules: { bsonType: 'array', items: { bsonType: 'string' } },
        sites: { bsonType: 'array', items: { bsonType: 'objectId' } },
        grants: {
          bsonType: 'array',
          items: {
            bsonType: 'object',
            required: [ 'site', 'module', 'action' ],
            additionalProperties: false,
            properties: {
              site: { bsonType: 'objectId' },
              module: { bsonType: 'string' },
              action: { bsonType: 'string' }
            }
          }
        }
      }
    },
    firstName: { bsonType: 'string' },
    lastName: { bsonType: 'string' },
    email: { bsonType: 'string' },
    emailVerified: { bsonType: 'bool' },
    locale: { bsonType: 'string' },
    timeZone: { bsonType: 'string' }
  }
}

/**
 * Indexes
 * Indexes used by MongoDB.
 * @type {array}
 */
const indexes = [{
  keys: { username: 1 },
  options: { background: true, unique: true, name: 'username_' }
}, {
  keys: { email: 1 },
  options: { background: true, unique: true, name: 'email_' }
}]

/**
 * Defaults
 * Overwrite BSON default values with the ones given here.
 * @type {object}
 */
const defaults = {
  locale: () => 'en',
  timeZone: () => 'auto',
  authentication: {
    accessTokenDuration: () => BSON.Long.fromNumber(2592000),
    blacklistable: () => false
  }
}

/**
 * Sortable
 * Properties that are sortable.
 * @type {array}
 */
const sortable = [
  '_id',
  'createAt',
  'username',
  'firstName',
  'lastName',
  'email'
]

/**
 * Filters
 * Filters that are applied to repository gets.
 * @type {object}
 */
const filters = {
  status: {
    process: PROCESS.FIRST,
    handler: value => {
      let filter
      if (isTrue(value)) {
        filter = { deleteAt: { $ne: null } }
      } else {
        filter = { deleteAt: { $eq: null } }
      }
      return filter
    }
  }
}

module.exports = new Schema(
  collectionName,
  schema,
  indexes,
  defaults,
  sortable,
  filters
)
