'use strict'

const fp = require('fastify-plugin')

const pluginName = 'hawk-schemas'

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    namespace: 'schemas'
  }, opts)

  // Create the cargo.
  const cargo = {
    blacklist: require('./blacklist'),
    site: require('./site'),
    user: require('./user'),
    common: require('./common')
  }

  // Decorate the instance accordingly.
  if (opts.namespace) {
    fastify.decorate(opts.namespace, cargo)
  } else {
    console.warn(`Using the \`${pluginName}\` plugin without a namespace is not recommended and may result in name collisions.`)
    for (let item in cargo) {
      fastify.decorate(item, cargo[item])
    }
  }
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: pluginName
})
