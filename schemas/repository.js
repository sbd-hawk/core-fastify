'use strict'

/**
 * Repository Schema
 * Sort, filter, and paginate options.
 */
const repository = {
  type: 'object',
  properties: {
    sortOn: {
      type: 'array',
      items: { type: 'string' },
      default: [ '_id' ]
    },
    page: { type: 'integer', default: 1, minimum: 1 },
    perPage: { type: 'integer', default: 100, minimum: 1, maximum: 500 }
  }
}

/**
 * Pagination Schema
 * Response with pagination information.
 */
const pagination = {
  type: 'object',
  properties: {
    page: { type: 'integer' },
    pages: { type: 'integer' },
    total: { type: 'integer' },
    perPage: { type: 'integer' }
  }
}
