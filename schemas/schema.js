'use strict'

const { BSON_TYPES } = require('../utils/convert')

/**
 * Enum for ways to process a filter.
 * 1. Accept multiples as an array,
 * 2. use first,
 * 3. use last, or
 * 4. process each individually.
 * @readonly
 * @enum {number}
 */
const PROCESS = {
  ARRAY: 1,
  FIRST: 2,
  LAST: 3,
  INDIVIDUALLY: 4
}

/**
 * Extract the property keys from a schema.
 * Schemas with objects will be namespaced.
 * Schemas with arrays with objects will be as well.
 * Does not handle JSON schema tuple validation.
 * @param {object} schema JSON schema.
 * @param {string} prefix Prefix to start with. Used in recursion.
 * @return {array} All namespaced keys.
 */
function keyifySchema (schema, prefix = '') {
  const keys = []
  for (let key in schema.properties) {
    let cur = schema.properties[key]
    let type = cur.bsonType || cur.type
    if (type === 'object') {
      keys.push(...keyifySchema(cur, prefix + key + '.'))
    } else if (type === 'array') {
      let itemType = cur.items.bsonType || cur.items.type
      if (itemType === 'object') {
        keys.push(...keyifySchema(cur.items, prefix + key + '.'))
      } else {
        keys.push(prefix + key)
      }
    } else {
      keys.push(prefix + key)
    }
  }
  return keys
}

/**
 * Create a default document from a schema.
 * @param {object} schema The schema to generate a document for.
 * @return {object} The default document.
 */
function defaultDocument (schema) {
  // Create the default document.
  const def = {}

  // Iterate over the properties.
  for (let prop in schema.properties) {
    // Get the type.
    let type
    if (Array.isArray(schema.properties[prop].bsonType)) {
      type = schema.properties[prop].bsonType[0]
    } else {
      type = schema.properties[prop].bsonType
    }

    // Handle nested object type cases.
    if (type === 'object') {
      def[prop] = defaultDocument(schema.properties[prop])
    } else {
      def[prop] = BSON_TYPES[type].default()
    }
  }

  return def
}

/**
 * Overwrite the values in an object with callables in a corresponding object.
 * @param {obj} object The object with values to be overwritten.
 * @param {over} object The object with callables to overwrite.
 * @return {object} The overwritten object.
 */
function overwrite (obj, over) {
  for (let prop in over) {
    if (typeof over[prop] === 'object') {
      obj[prop] = overwrite(obj[prop], over[prop])
    } else {
      obj[prop] = over[prop]()
    }
  }
  return obj
}

/**
 * Get a subset of a schema.
 * @param {object} schema The schema to subset.
 * @param {object} include The fields to include.
 * @return {object} The subset of the schema.
 */
function subsetSchema (schema, include) {
  // Base case.
  if (schema.bsonType !== 'object') {
    return JSON.parse(JSON.stringify(schema))
  }

  // Initialize the new subschema.
  const subschema = { bsonType: schema.bsonType, properties: {} }

  // Iterate over the included properties and add them to the subschema.
  for (let prop in include) {
    // If the property is an object, recurse.
    if (schema.properties[prop].bsonType === 'object') {
      if (isObject(include[prop])) {
        subschema.properties[prop] = subsetSchema(schema.properties[prop], include[prop])
      } else {
        subschema.properties[prop] = {
          bsonType: schema.properties[prop].bsonType,
          properties: JSON.parse(JSON.stringify(schema.properties[prop].properties))
        }
      }
    } else {
      subschema.properties[prop] = subsetSchema(schema.properties[prop], {})
    }
  }

  return subschema
}

/**
 * Convert a bson schema to json schema.
 * @param {object} schema The schema to convert.
 * @return {object} The converted schema.
 */
function bsonToJSONSchema (schema) {
  // Convert the schema to a string.
  const json = JSON.stringify(schema)

  // Change bsonType to type and the values themselves.
  return JSON.parse(json.split('bsonType').map((cur, index) => {
    // Skip the first item.
    if (index === 0) {
      return cur
    }

    // Handle single and array types.
    let to
    let original
    let jsonType
    if (cur.slice(2, 3) === '[') {
      to = cur.slice(3).indexOf(']') + 4
      original = cur.slice(2, to)
      const types = JSON.parse(original).map(type => BSON_TYPES[type].json)
      jsonType = cur.split(original).join(JSON.stringify(types))
    } else {
      to = cur.slice(3).indexOf('"') + 3
      original = cur.slice(3, to)
      const type = BSON_TYPES[original].json
      jsonType = cur.split(original).join(type)
    }
    return jsonType
  }).join('type'))
}

class Schema {
  /**
   * Create a new instance of Schema.
   * @param {string} collectionName
   * @param {object} schema
   * @param {array} indexes
   * @param {object} defaults
   * @param {array} sortable
   * @param {object} filters
   */
  constructor (
    collectionName,
    schema,
    indexes = [],
    defaults = {},
    sortable = [],
    filters = {}
  ) {
    // Create instance variables.
    this.collectionName = collectionName
    this.schema = schema
    this.indexes = indexes
    this.defaults = defaults
    this.sortable = sortable
    this.filters = filters

    // Validate some of the input.
    for (let i = 0; i < sortable.length; i++) {
      if (!(sortable[i] in schema.properties)) {
        throw new Error(`Property '${sortable[i]}' given in 'sortable' not in schema`)
      }
    }
  }

  /**
   * Prime the collection in the database.
   * @param {} db The database instance.
   */
  async primeCollection (db) {
    // Create the collection.
    const collection = await db.createCollection(this.collectionName)

    // Add the validator.
    await db.command({
      collMod: collection.collectionName,
      validator: { $jsonSchema: this.schema }
    })

    // Create the indexes.
    for (let i = 0; i < this.indexes.length; i++) {
      await collection.createIndex(this.indexes[i].keys, this.indexes[i].options)
    }
  }

  /**
   * Get a new default document.
   */
  get defaultDocument () {
    return overwrite(defaultDocument(this.schema), this.defaults)
  }

  /**
   * Extract only the properties given.
   * @param {object} include Properties to extract in subschema.
   * @return {object} JSON schema with only the properties given. Also any
   *  extra top level properties are dropped.
   */
  subsetSchema (include, typeKind = 'json') {
    let subschema = subsetSchema(this.schema, include)
    if (typeKind === 'json') {
      subschema = bsonToJSONSchema(subschema)
    }
    return subschema
  }
}

module.exports = { PROCESS, Schema }
