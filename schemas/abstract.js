'use strict'

const USERNAME_RE = "(?=^[a-zA-Z0-9\\-\\_\\.]{4,20}?$)(?=^[a-zA-Z0-9].*[a-zA-Z0-9]$)(?!^.*[\\-\\_\\.]{2,}.*$)"
const PASSWORD_RE = "(?=^.{8,}$)(?=^[^\\s]+.*[^\\s]+$)"

// Abstract Schemas
// ----------------
/**
 * Credentials Schema
 * Object containing a user's credentials.
 */
const credentials = {
  type: 'object',
  required: [ 'username', 'password' ],
  properties: {
    username: { type: 'string', pattern: USERNAME_RE },
    password: { type: 'string', pattern: PASSWORD_RE }
  }
}

/**
 * Repository Schema
 * Sort, filter, and paginate options.
 */
const repository = {
  type: 'object',
  properties: {
    filterKey: {
      type: 'array',
      items: { type: 'string' },
      default: []
    },
    filterValue: {
      type: 'array',
      items: { type: 'string' },
      default: []
    },
    returnFilters: { type: 'boolean', default: false },
    sortOn: {
      type: 'array',
      items: { type: 'string' },
      default: [ '_id' ]
    },
    page: { type: 'integer', default: 1, minimum: 1 },
    perPage: { type: 'integer', default: 100, minimum: 1, maximum: 500 }
  }
}

/**
 * Pagination Schema
 * Response with pagination information.
 */
const pagination = {
  type: 'object',
  properties: {
    page: { type: 'integer' },
    pages: { type: 'integer' },
    total: { type: 'integer' },
    perPage: { type: 'integer' }
  }
}

module.exports = { credentials, repository, pagination }
