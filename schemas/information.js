'use strict'

// Complete Schemas
// ----------------
/**
 * GET /status
 */
const getStatus = {
  response: {
    200: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        version: { type: 'string' },
        status: { type: 'string' },
        creator: {
          type: 'object',
          properties: {
            name: { type: 'string' },
            email: { type: 'string' }
          }
        },
        stack: {
          type: 'array',
          items: { type: 'string' }
        },
        time: { type: 'integer' }
      }
    }
  }
}

/**
 * GET /source
 */
const getSource = {
  response: {
    304: {
      type: 'null'
    }
  }
}

module.exports = { getStatus, getSource }
