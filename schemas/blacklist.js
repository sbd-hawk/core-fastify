'use strict'

const { schema } = require('./schema')

/**
 * Session Blacklist Schema
 * Used at every request, if the user is blacklistable.
 * _id is a JTI asigned to a JWT (UUIDv4).
 */
const sessionBlacklist = {
  collectionName: 'sessionBlacklist',
  schema: {
    bsonType: 'object',
    required: [ '_id' ],
    additionalProperties: false,
    properties: {
      _id: { bsonType: 'string' }
    }
  }
}

/**
 * IP Blacklist Schema
 * Used when creating a session.
 * _id is an IP address.
 */
const ipBlacklist = {
  collectionName: 'ipBlacklist',
  schema: {
    bsonType: 'object',
    required: [ '_id' ],
    additionalProperties: false,
    properties: {
      _id: { bsonType: 'string' }
    }
  }
}

module.exports = { sessionBlacklist, ipBlacklist }
