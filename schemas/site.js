'use strict'

const { Schema } = require('./schema')

/**
 * Collection Name
 * Name of the collection in MongoDB.
 * @type {string}
 */
const collectionName = 'sites'

/**
 * Schema
 * JSON Schema used to validate documents in the database.
 * @type {object}
 */
const schema = {
  bsonType: 'object',
  required: [ '_id', 'createAt', 'updateAt', 'deleteAt', 'name', 'title', 'location' ],
  additionalProperties: false,
  properties: {
    _id: { bsonType: 'objectId' },
    createAt: { bsonType: 'date' },
    updateAt: { bsonType: 'date' },
    deleteAt: { bsonType: [ 'null', 'date' ] },
    name: { bsonType: 'string' },
    title: { bsonType: 'string' },
    location: {
      bsonType: ['null', 'object' ],
      required: [ 'type', 'coordinates' ],
      properties: {
        type: { bsonType: 'string', enum: [ 'Point' ] },
        coordinates: {
          bsonType: 'array',
          items: { bsonType: 'double' },
          minItems: 2,
          maxItems: 2
        }
      }
    },
    timeZone: { bsonType: 'string' }
  }
}

/**
 * Indexes
 * Indexes used by MongoDB.
 * @type {array}
 */
const indexes = [{
  keys: { name: 1 },
  options: { background: true, unique: true, name: 'name_' }
}]


module.exports = new Schema(
  collectionName,
  schema,
  indexes
)
