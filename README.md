# Hawk Core
This is the backend for the Hawk Core module of the Hawk application.
The primary purpose of this module is to provide management of data relating to core features.
Core features include the management of users, sites, languages, etc. and the creation of sessions.
This will act as the authentication server for all Hawk modules.

## Endpoints
Here is an overview of the endpoints.

### `GET /users`
This returns all users currently in the database.

#### Query Parameters
The following parameters will be searched for in the query string.

##### `filterKey`
The `filterKey` parameter gives a key to filter.
This key must be defined on some document in the database.
Multiple values are allowed.
An implicit `or` is performed within keys and an implicit `and` is performed between keys.

##### `filterValue`
The `filterValue` parameter gives the value to filter on.
`filterKey` and `filterValue` are matched based on their order in the query string.
Multiple values are allowed.

##### `sortOn`
The `sortOn` parameter gives the key that should be sorted.
To set the direction, prepend the value with either `+` or `-` to set ascending or descending, respectively.
The default sort direction is ascending.
Multiple values are allowed.
The value of this parameter will affect the way paging is done.
If the first value given is a unique indexed key then the (`lastID`, `limit`) paging scheme will be used, otherwise the (`page`, `limit`) paging scheme is used.
If multiple values are given and the first `sortOn` value is a unique indexed key, the subsequent `sortOn` values will be ignored, as they do not affect the sorting.

* Default: `_id`

##### `lastID`
The `lastID` parameter is the indexing value of the last returned document
It is used in the (`lastID`, `limit`) paging scheme, which is faster than the (`page`, `limit`) paging scheme for large collections.
It must be a value from the key given in `sortOn`.
It is only relevent when the first value for `sortOn` is a unique indexed value, which is true by default as `sortOn=_id`.
For example, `_id` and `username` are unique indexed, but `firstName` is not, so if `firstName` is given in `sortOn`, this will be ignored.

##### `page`
The `page` parameter is the desired page to fetch.
It is used in the (`page`, `limit`) paging scheme, which is slower that the (`lastID`, `limit`) paging scheme for large collections.
If the value in `sortOn` is anything other than a unique indexed key, then it will be used.

* Default: `1`

##### `limit`
The `limit` parameter represents the number of documents fetched per page.

* Default: `100`

##### Examples
1. Sort by `username` descending with `10` documents per page: `?sortOn=-username&limit=10`
1. Filter for users with English default: `?filterKey=language&filterValue=en`
1. Filter for admins with access to a the `ehs` module and sort by their last name: `?filterKey=role&filterValue=admin&filterKey=module&filterValue=ehs&sortOn=lastName`

#### Responses
```json

```
