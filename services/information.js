'use strict'

const schemas = require('../schemas/information')

/**
 * GET /status
 * Get the status of the application.
 */
async function getStatus (req, reply) {
  reply
    .code(200)
    .type('application/json')
    .send({
      name: 'Hawk Core',
      version: this.version,
      status: 'running',
      creator: {
        name: 'Andrew C. Hawkins',
        email: 'andrew.hawkins@sbdinc.com'
      },
      stack: [ 'node.js', 'fastify', 'mongodb' ],
      time: new Date()
    })
}

/**
 * GET /source
 * Get the source of the application.
 */
async function getSource (req, reply) {
  reply.redirect('https://gitlab.com/sbd-hawk/core-fastify')
}

module.exports = async function (fastify, opts) {
  fastify.get('/status', { schema: schemas.getStatus }, getStatus)
  fastify.get('/source', { schema: schemas.getSource }, getSource)
}
