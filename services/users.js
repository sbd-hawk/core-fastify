'use strict'

const BSON = require('bson')
const routes = {
  repository: '/',
  specific: '/:userID(^[0-9a-fA-F]+$)',
  me: '/me'
}
const schemas = require('../schemas/user')

/**
 * POST /users
 * Create a new user.
 */
async function postUsers (req, reply) {
  // Create the new user.
  const user = await this.daos.users.create({
    username: req.body.username,
    password: await this.authentication.hashPassword(req.body.password)
  })

  reply
    .code(201)
    .type('application/json')
    .send(user)
}

/**
 * GET /users/me
 * Get the self user as given in the JWT.
 */
async function getUserMe (req, reply) {
  reply.notImplemented()
}

/**
 * GET /users/:userID
 * Get a single user.
 */
async function getUser (req, reply) {
  // Fetch the user.
  const user = await this.daos.users.getByID(req.params.userID)

  reply
    .code(200)
    .type('application/json')
    .send(user)
}

/**
 * PATCH /users/:userID
 * Update a user's information.
 */
async function patchUser (req, reply) {
  // Extract the update.
  const update = req.body

  // Hash the password if given.
  if ('password' in update) {
    update.password = await this.authentication.hashPassword(update.password)
  }

  // Check if admin only fields are given.
  const givenAdminProperties = Object.keys(update).filter(cur => schemas.USER.adminPropertySet.has(cur))
  if (givenAdminProperties.length > 0) {
    const decoded = await req.jwtVerify()
    console.log(decoded)
  }

  // Update the user.
  const user = await this.daos.users.update(req.params.userID, update)

  reply
    .code(200)
    .type('application/json')
    .send(user)
}

/**
 * DELETE /users/:userID
 * Delete a user.
 */
async function deleteUser (req, reply) {
  // Deactivate the user.
  await this.daos.users.deactivate(req.params.userID)

  reply
    .code(204)
    .send()
}

module.exports = async function (fastify, opts) {
  /**
   * Check if the requesting user is an admin.
   */
  async function adminOnly (req, claims) {
    const user = await fastify.daos.users.getByID(BSON.ObjectID(claims.sub))
    return user.role === 'admin'
  }

  /**
   * Check if the requesting user is an admin or owner.
   */
  async function adminOrOwner (req, claims) {
    const user = await fastify.daos.users.getByID(BSON.ObjectID(claims.sub))
    return user.role === 'admin' || req.params.userID.equals(user._id)
  }

  // Unauthenticated routes.
  fastify.post(routes.repository, { schema: schemas.postUsers }, postUsers)

  // Admin only routes.
  fastify.register(async function (fastify) {
    fastify.addHook(
      'preHandler',
      fastify.authorization.requiresJWT(fastify.authorization.checks.adminOnly))
    fastify.get(
      routes.repository,
      { schema: fastify.repository.schema(schemas.USER) },
      fastify.repository.handler(schemas.USER.name)
    )
  })

  // Admin or owner routes.
  fastify.register(async function (fastify) {
    fastify.addHook('preHandler', fastify.authorization.requiresJWT(adminOrOwner))
    fastify.get(routes.specific, { schema: schemas.getUser }, getUser)
    fastify.get(routes.me, { schema: schemas.getUser }, getUserMe)
    fastify.patch(routes.specific, { schema: schemas.patchUser }, patchUser)
    fastify.delete(routes.specific, { schema: schemas.deleteUser }, deleteUser)
  })

  // Handle errors.
  fastify.setErrorHandler(function (err, req, reply) {
    // Handle validation errors.
    if ('validation' in err) {
      if (err.validation[0].dataPath === '.userID') {
        reply.code(400)
        err.message = 'not a valid user id'
      }
    }

    // Handle mongodb errors.
    if ('code' in err) {
      switch (err.code) {
        case 11000:
          // Username is taken.
          reply.code(400)
          err.message = 'requested username is taken'
          break
        case 60000:
          // Document id does not exist.
          reply.code(400)
          break
      }
    }
    reply.send(err)
  })
}
