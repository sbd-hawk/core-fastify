'use strict'

const routes = {
  repository: '/',
  specific: '/:sessionID(^[0-9a-fA-F]+$)',
  me: '/me'
}
const schemas = require('../schemas/session')

/**
 * POST /sessions
 * Create a new session.
 */
async function postSessions (req, reply) {
  // Authenticate the request.
  const { userID: sub, auth } = await this.daos.users.authenticate(req.body.username, req.body.password)

  // Create the JWT internals.
  const jti = this.convert.toObjectID()
  const iat = Math.floor(new Date().getTime() / 1000)
  const exp = iat + auth.jwtDuration

  // Create and sign the JWT.
  const jwt = await reply.jwtSign({ jti, sub, iat, exp })

  // Create the new session.
  const session = await this.daos.sessions.create({
    _id: jti,
    createAt: new Date(iat * 1000),
    expireAt: new Date(exp * 1000),
    userID: sub,
    ip: req.ip
  })

  reply
    .setCookie('token', jwt, {
      httpOnly: true,
      // secure: true,
      sameSite: true
    })
    .code(201)
    .type('application/json')
    .send(session)
}

/**
 * GET /sessions/:sessionID
 * Get a single session.
 */
async function getSession (req, reply) {
  // Fetch the session.
  const session = await this.daos.sessions.getByID(req.params.sessionID)

  reply
    .code(200)
    .type('application/json')
    .send(session)
}

/**
 * DELETE /sessions/:sessionID
 * End a session.
 */
async function deleteSession (req, reply) {
  // End the session.
  await this.daos.sessions.end(req.params.sessionID)

  reply
    .code(204)
    .send()
}

module.exports = async function (fastify, opts) {
  // Unauthenticated routes.
  fastify.post(
    routes.repository,
    { schema: schemas.postSessions },
    postSessions
  )

  // Admin only routes.
  fastify.register(async function (fastify) {
    //fastify.addHook('preHandler', fastify.authPreHandler('admin'))
    fastify.get(
      routes.repository,
      { schema: fastify.repository.schema(schemas.SESSION) },
      fastify.repository.handler(schemas.SESSION.name)
    )
  })

  // Admin or owner routes.
  fastify.register(async function (fastify) {
    //fastify.addHook('preHandler', fastify.authPreHandler('either'))
    fastify.get(
      routes.specific,
      { schema: schemas.getSession },
      getSession
    )
    fastify.delete(
      routes.specific,
      {schema: schemas.deleteSession },
      deleteSession
    )
  })

  fastify.setErrorHandler(function (err, req, reply) {
    reply.send(err)
  })
}
