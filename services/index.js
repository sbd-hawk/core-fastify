'use strict'

const fp = require('fastify-plugin')

async function routes (fastify, opts) {
  fastify.register(require('./information'))
  fastify.register(require('./users'), { prefix: '/users' })
  fastify.register(require('./sessions'), { prefix: '/sessions' })
}

async function plugin (fastify, opts) {
  // Set default options.
  opts = Object.assign({
    version: [1, 0, 0],
    basePath: fastify.config.BASE_PATH,
    versionInPath: fastify.config.VERSION_IN_PATH
  }, opts)

  // Set the prefix.
  const prefix = opts.basePath + (opts.versionInPath ? '/v' + opts.version[0] : '')

  // Register the routes.
  fastify.register(routes, { prefix })
}

module.exports = fp(plugin, {
  fastify: '2.x',
  name: 'hawk-services',
  dependencies: [
    'fastify-env',
    'fastify-mongodb',
    'fastify-authentication',
    'fastify-authorization',
    'fastify-convert',
    'hawk-daos'
  ]
})
