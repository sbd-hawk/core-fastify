'use strict'

const BSON = require('bson')
//const fp = require('fastify-plugin')

/**
 * Do nothing to the input. Used as a callable that does nothing.
 * @param {any} value Some value.
 * @return {any} The value echoed.
 */
const identity = value => value

/**
 * Convert an unix timestamp to a Date object.
 * @param {number} unix The unix timestamp to convert.
 * @return {Date} The converted date object.
 */
const unixToDate = unix => new Date(unix * 1000)

/**
 * Convert a unix millisecond timestamp to a Date object.
 * @param {number} unixMillis The unix millisecond timestamp to convert.
 * @return {Date} The converted date object.
 */
const unixMillisToDate = unixMillis => new Date(unixMillis)

/**
 * Convert a Date object to a unix timestamp.
 * @param {Date} date The date object to convert.
 * @return {number} The unix timestamp.
 */
const dateToUnix = date => Math.floor(date.getTime() / 1000)

/**
 * Convert a Date object to a unix millisecond timestamp.
 * @param {Date} date The date object to convert.
 * @return {number} The unix millisecond timestamp.
 */
const dateToUnixMillis = date => date.getTime()

/**
 * Check if a variable is a proper object.
 * @param {any} value Value to check if it is a proper object.
 * @return {boolean} Whether the value is a proper object.
 */
function isObject (value) {
  return value && typeof value === 'object' && value.constructor === Object
}

/**
 * Strings that should evaluate to true.
 * @readonly
 * @type {set}
 */
const _trueStrings = new Set([ 'true', 't', '1' ])

/**
 * Check if a string value is true.
 * @param {string} value The string to check.
 * @param {set} possibilities The possible true strings.
 * @return {boolean} If the string was true.
 */
function isTrue (value, possibilities = _trueStrings) {
  return possibilities.has(value.toLowerCase())
}

/**
 * Strings that should evaluate to false.
 * @readonly
 * @type {set}
 */
const _falseStrings = new Set([ 'false', 'f', '0' ])

/**
 * Check if a string value is false.
 * @param {string} value The string to check.
 * @param {set} possibilities The possible true strings.
 * @return {boolean} If the string was false.
 */
function isFalse (value, possibilities = _falseStrings) {
  return possibilities.has(value.toLowerCase())
}

/**
 * Convert between BSON and JSON types.
 */
const BSON_TYPES = {
  object: {
    json: 'object',
    fromJSON: identity,
    toJSON: identity,
    default: () => { return {} }
  },
  array: {
    json: 'array',
    fromJSON: identity,
    toJSON: identity,
    default: () => []
  },
  string: {
    json: 'string',
    fromJSON: identity,
    toJSON: identity,
    default: () => ''
  },
  objectId: {
    json: 'string',
    fromJSON: BSON.ObjectID,
    toJSON: v => v.toString(),
    default: () => BSON.ObjectID()
  },
  bool: {
    json: 'boolean',
    fromJSON: identity,
    toJSON: identity,
    default: () => false
  },
  date: {
    json: 'integer',
    fromJSON: unixToDate,
    toJSON: dateToUnix,
    default: () => new Date()
  },
  int: {
    json: 'integer',
    fromJSON: identity,
    toJSON: identity,
    default: () => 0
  },
  long: {
    json: 'number',
    fromJSON: BSON.Long.fromNumber,
    toJSON: v => v.toNumber(),
    default: () => BSON.Long.fromNumber(0)
  },
  double: {
    json: 'number',
    fromJSON: BSON.Double,
    toJSON: v => v.valueOf(),
    default: () => BSON.Double(0)
  },
  null: {
    json: 'null',
    fromJSON: identity,
    toJSON: identity,
    default: () => null
  }
}

module.exports = { BSON_TYPES, isTrue, isFalse }
