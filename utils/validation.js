'use strict'

const Ajv = require('ajv')

const { BSON_TYPES } = require('./convert')

// Configure AJV.
const ajv = new Ajv({
  removeAdditional: true,
  useDefaults: true,
  coerceTypes: 'array',
  allErrors: true
})

// Helpers
// -------
/**
 * Get the nested type. If the type is an array look for its items.
 * @param {object} obj The object to examine.
 * @param {string} typeName The property name of type. Usually `bsonType` or `type`.
 * @return {string} The type.
 */
function getNestedType (obj, typeName = 'bsonType') {
  const type = obj[typeName]
  if (type !== 'array') return type
  if (!('items' in obj)) throw new Error('if type is `array` it must contain `items`')
  return getNestedType(obj.items, typeName)
}

/**
 * Transform the raw sort to a mongodb sortable.
 * @param {array} sortOn The fields to sort on. Ordered by sort preference. Optionally prepend + or - for sort direction.
 * @param {Set} sortable A set of the sortable keys.
 * @return {array} A sort array with key and direction.
 */
function transformSort (sortOn, sortable) {
  return sortOn.map(cur => {
    let sort
    if (cur[0] === '+') {
      sort = [ cur.slice(1), 1 ]
    } else if (cur[0] === '-') {
      sort = [ cur.slice(1), -1 ]
    } else {
      sort = [ cur, 1 ]
    }
    if (!sortable.has(sort[0])) throw new Error(`\`sortOn\` \`${sort[0]}\` is not sortable`)
    return sort
  })
}

/**
 * Transform the raw filter keys and values to a mongodb query.
 * @param {array} filterKey The keys to filter on. Position corresponds with value.
 * @param {array} filterValue The values to filter on. Position corresponds with key.
 * @param {Set} filterable A set of the filterable keys.
 * @param {object} schemaProperties The schema properties for a document in the repository.
 * @return {object} The query object as given by the inputs.
 */
function transformFilters (filterKey, filterValue, filterable, schemaProperties) {
  // Make sure the containers are the same length.
  if (filterKey.length !== filterValue.length) {
    throw new Error('`filterKey` and `filterValue` must be the same length')
  }

  // Create the query.
  const filters = filterKey.reduce((acc, cur, idx) => {
    // Check if the passed filter key is filterable.
    if (!filterable.has(cur)) {
      throw new Error(`\`filterKey\` \`${cur}\` is not filterable`)
    }

    // Get the bsonType item type. //! Supports uniform type arrays only!
    const bsonType = getNestedType(schemaProperties[cur])

    // Convert the json value to bson, if other than objectId or string do JSON parse.
    //! Remember the values in `filterValue` are always parsed as strings!
    //! Should check out what happens if `bsonType` is `object`. Maybe should throw error?
    //! `bsonType` cant be array because of how `getNestedType` works.
    let rawValue = filterValue[idx]
    if (![ 'objectId', 'string' ].includes(bsonType)) {
      rawValue = JSON.parse(rawValue)
    }
    const value = BSON_TYPES[bsonType].fromJSON(rawValue)
    if (cur in acc) {
      acc[cur].push(value)
    } else {
      acc[cur] = [ value ]
    }
    return acc
  }, {})

  // Convert array filters to a $in query.
  for (let field in filters) {
    if (filters[field].length === 1) {
      filters[field] = filters[field][0]
    } else {
      filters[field] = { $in: filters[field] }
    }
  }

  return filters
}

// Custom Keywords
// ---------------
/**
 * `repositorySchema` custom keyword.
 * Accepts the schema of a document in the repository.
 * Expects `sortable` and `filterable` keywords to be set on the `repositorySchema`.
 * Those keywords are information and are not validated, there is a meta schema for them.
 */
ajv.addKeyword('repositorySchema', {
  compile: function (schema, parentSchema) {
    // Verify properties include `sortOn`, `filterKey`, and `filterValue`.
    if (!('sortOn' in parentSchema.properties)) {
      throw new Error('repositorySchema must be defined with `sortOn` in the properties')
    } else if (!('filterKey' in parentSchema.properties)) {
      throw new Error('repositorySchema must be defined with `filterKey` in the properties')
    } else if (!('filterValue' in parentSchema.properties)) {
      throw new Error('repositorySchema must be defined with `filterValue` in the properties')
    }

    // Verify `bsonType` is in each property of the schema.
    for (let field in schema.properties) {
      if (!('bsonType' in schema.properties[field])) {
        throw new Error('`bsonType` must be declared the schema properties')
      }
    }

    // Verify `sortable` and `filterable` contain acutal properties.
    if (Array.isArray(schema.sortable)) {
      schema.sortable.forEach(cur => {
        if (!(cur in schema.properties)) throw new Error(`\`sortable\` value \`${cur}\` not found in \`properties\``)
      })
    }
    if (Array.isArray(schema.filterable)) {
      schema.filterable.forEach(cur => {
        if (!(cur in schema.properties)) throw new Error(`\`filterable\` value \`${cur}\` not found in \`properties\``)
      })
    }

    // Set sortable and filterable sets for fast checking.
    const sortable = new Set(schema.sortable === 'all' ? Object.keys(schema.properties) : schema.sortable)
    const filterable = new Set(schema.filterable === 'all' ? Object.keys(schema.properties) : schema.filterable)
    sortable.add('_id')

    // Return the validation function.
    return function (data) {
      // Validate the sort.
      try {
        data.sort = transformSort(data.sortOn, sortable)
      } catch (e) {
        return false
      }

      // Validate the filters.
      try {
        data.filters = transformFilters(data.filterKey, data.filterValue, filterable, schema.properties)
      } catch (e) {
        return false
      }

      return true
    }
  },
  metaSchema: {
    type: 'object',
    required: [ 'sortable', 'filterable' ],
    properties: {
      sortable: { type: 'array', items: { type: 'string' } },
      filterable: { type: 'array', items: { type: 'string' } },
      properties: { type: 'object' }
    }
  }
})

/**
 * `bsonType` custom keyword
 * Convert the json type to a bson type.
 */
ajv.addKeyword('bsonType', {
  compile: function (schema, parentSchema) {
    // Check if the types match.
    if (BSON_TYPES[schema].json !== parentSchema.type) {
      throw new Error(`bsonType '${schema}' must be accompanied with type '${BSON_TYPES[schema].json}'`)
    }

    // Return the validation function.
    return function (data, dataPath, parentData, parentProperty) {
      // Try to convert the type.
      try {
        parentData[parentProperty] = BSON_TYPES[schema].fromJSON(data)
      } catch (e) {
        return false
      }
      return true
    }
  },
  metaSchema: {
    type: 'string',
    enum: Object.keys(BSON_TYPES)
  },
  dependencies: [ 'type' ]
})

module.exports = ajv
