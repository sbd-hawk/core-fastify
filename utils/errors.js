'use strict'

class APIError extends Error {
  constructor (message, code) {
    super(message)
    this.code = code
  }
}

const ERRORS = {
  DOC_DOES_NOT_EXIST: new APIError('document does not exist', 60000),
  BAD_CREDENTIALS: new APIError('incorrect credentials provided', 60001)
}

module.exports = ERRORS
