'use strict'

const Ajv = require('ajv')

const { BSON_TYPES } = require('./convert')

/**
 * Create the validator for a Schema `property`.
 */
const ajv = new Ajv({ removeAdditional: true, useDefaults: true })
const validateProperty = ajv.compile({
  type: 'object',
  required: [ 'name', 'bsonType' ],
  properties: {
    name: { type: 'string' },
    bsonType: { type: 'string', enum: Object.keys(BSON_TYPES) },
    required: { type: 'boolean', default: false },
    sortable: { type: 'boolean', default: false },
    filterable: { type: 'boolean', default: false },
    nullable: { type: 'boolean', default: false },
    intent: {
      type: [ 'string', 'null' ],
      enum: [ 'in', 'out', 'inout', null ],
      default: 'inout'
    },
    admin: { type: 'boolean', default: false },
    default: { default: null },
    items: {
      type: [ 'string', 'null' ],
      enum: [...Object.keys(BSON_TYPES), null],
      default: null
    },
    schema: { default: null }
  }
})


class Schema {
  /**
   * Create an instance of schema.
   * @param {string} name The name of the schema.
   * @param {boolean} topLevel If the schema belongs to a collection.
   */
  constructor (name, topLevel) {
    // Validate input.
    if (topLevel && !name) throw new Error('all top level schemas must have collection names')

    // Initialize instance variables.
    this.topLevel = topLevel
    this.name = name
    this.propertySet = new Set()
    this.adminPropertySet = new Set()
    this.properties = []
    this.indexSet = new Set()
    this.indexes = []
    this.indexLock = new Set()
  }

  /**
   * Add a property to the schema.
   * @param {object} property Information about the property.
   * @param {string} property.name The name of the property.
   * @param {string} property.bsonType The BSON type of the property.
   * @param {boolean} property.required If the property is required in the database.
   * @param {boolean} property.sortable If the property can be sorted on.
   * @param {boolean} property.filterable If the property can be used in a filter.
   * @param {boolean} property.nullable If the property can be null.
   * @param {string} property.intent The availablity in input and output. Possible values: 'in', 'out', 'inout', null.
   * @param {boolean} property.admin If the property can only be updated by an admin.
   * @param {function} property.default A callable that provides the default value, used for default model generation.
   * @param {string} property.items If `property.bsonType` is 'array', the BSON type of values in array.
   * @param {Schema} property.schema If `property.bsonType` or `property.items` is 'object', the schema of the object. Must be a non top level schema.
   */
  addProperty (property) {
    // Validate the property.
    if (!validateProperty(property)) {
      throw new Error(validateProperty.errors)
    }

    // Check if the name has been defined.
    if (this.propertySet.has(property.name)) {
      throw new Error(`property ${property.name} already defined`)
    }

    // Check items is given if array.
    if (property.bsonType === 'array' && !property.items) {
      throw new Error('`items` must accompany a bsonType `array`')
    }

    // Check schema is given if object.
    if (property.bsonType === 'object') {
      if (!property.schema) {
        throw new Error('`schema` must accompany a bsonType `object`')
      }
      if (property.schema.constructor.name !== 'Schema') {
        throw new Error('`schema` for `bsonType` `object` must be a `Schema`')
      }
      if (property.schema.topLevel) {
        throw new Error('cannot add a top level schema as a property schema')
      }
    }

    // Check the type of default is callable.
    if (property.default) {
      if (typeof property.default !== 'function') {
        throw new Error('`default` must be a callable')
      }
    } else {
      // Set the default if null is given.
      property.default = BSON_TYPES[property.bsonType].default
    }

    // If the property is for admins only add it to the set.
    if (property.admin) this.adminPropertySet.add(property.name)

    // Add the property.
    this.propertySet.add(property.name)
    this.properties.push(property)
  }

  /**
   * Remove a property from the schema.
   * @param {string} name The name of the property to remove.
   */
  removeProperty (name) {
    // Find the index of the property.
    const index = this.properties.find(cur => cur.name === name)

    // Throw an error if it doesnt exist.
    if (index === undefined) {
      throw new Error(`property '${name}' does not exist`)
    }

    // Check if an index relies on this property.
    if (this.indexLock.has(name)) {
      throw new Error(`'${name}' is indexed, remove index first`)
    }

    // Remove the property.
    this.propertySet.delete(name)
    this.properties.splice(index, 1)
  }

  /**
   * Add an index to the schema.
   * @param {string} name The name of the index.
   * @param {object} keys Object mapping keys to direction (1: asc, -1: desc).
   * @param {object} opts Extra options for the index.
   */
  addIndex (name, keys, opts = {}) {
    // Validate input.
    if (this.indexSet.has(name)) {
      throw new Error(`index ${name} already defined`)
    }
    for (let key in keys) {
      if (!this.propertySet.has(key)) {
        throw new Error(`key '${key}' not a valid property`)
      }
      if (!(keys[key] === 1 || keys[key] === -1)) {
        throw new Error(`key '${key}' direction '${keys[key]}' not available`)
      }
    }

    // Create the options.
    const options = Object.assign({ name }, opts)

    // Add the index.
    this.indexSet.add(name)
    this.indexes.push({ keys, options })
  }

  /**
   * Remove an index from the schema.
   */
  removeIndex (name) {
    // Find the index of the index.
    const index = this.indexes.find(cur => cur.options.name === name)

    // Throw an error if it doesnt exist.
    if (index === undefined) {
      throw new Error(`index '${name}' does not exist`)
    }

    // Remove the index.
    this.indexSet.delete(name)
    this.indexes.splice(index, 1)
  }

  /**
   * Get the mongo schema.
   * @return {object} The schema for mongodb has three properties:
   *   @param {string} name name of the collection
   *   @param {object} validator $jsonSchema
   *   @param {array} indexes All indexes.
   */
  get mongoSchema () {
    return this.topLevel ? {
      name: this.name,
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: this.properties.filter(cur => cur.required).map(cur => cur.name),
          properties: this._subsetProperties('all', 'bson')
        }
      },
      indexes: this.indexes
    } : undefined
  }

  /**
   * Get a subset of the properties, based on the intent.
   * @param {string} intent The intent to get.
   * @param {string} typeKind The type to use, can be `bson` or `json`.
   * @return {object} The parameters as keys with their
   */
  _subsetProperties (intent, typeKind = 'json') {
    // Validate input.
    if (![ 'in', 'out', 'inout', null, 'all' ].includes(intent)) {
      throw new Error(`intent '${intent}' not supported`)
    }
    if (![ 'bson', 'json' ].includes(typeKind)) {
      throw new Error(`typeKind '${typeKind}' not supported`)
    }

    // Set what to check.
    const checks = new Set()
    if (intent === 'all') {
      checks.add('in').add('out').add('inout').add(null)
    } else if (intent === 'in' || intent === 'out') {
      checks.add('inout').add(intent)
    } else if (intent === 'inout') {
      checks.add('in').add('out').add('inout')
    } else {
      checks.add(null)
    }

    // Create the properties object.
    return this.properties.reduce((acc, cur) => {
      if (checks.has(cur.intent)) {
        const type = typeKind === 'bson' ? cur.bsonType : BSON_TYPES[cur.bsonType].json
        acc[cur.name] = {}
        acc[cur.name][typeKind === 'bson' ? 'bsonType' : 'type'] = cur.nullable ? [ 'null', type ] : type
        switch (cur.bsonType) {
          case 'object':
            acc[cur.name].properties = cur.schema._subsetProperties(intent, typeKind)
            break
          case 'array':
            const itemsType = typeKind === 'bson' ? cur.items : BSON_TYPES[cur.items].json
            acc[cur.name].items = {}
            acc[cur.name].items[typeKind === 'bson' ? 'bsonType' : 'type'] = itemsType
            break
        }
      }
      return acc
    }, {})
  }

  /**
   * Get the input properties of the schema.
   */
  get inputProperties () {
    return this._subsetProperties('in', 'json')
  }

  /**
   * Get the output properties of the schema.
   */
  get outputProperties () {
    return this._subsetProperties('out', 'json')
  }

  /**
   * Generate a model based on the default values.
   */
  get defaultModel () {
    return this.properties.reduce((acc, cur) => {
      acc[cur.name] = cur.bsonType === 'object' ? cur.schema.defaultModel : cur.default()
      return acc
    }, {})
  }

  /**
   * Get the schema in the format used by repository validator.
   */
  get schema () {
    return {
      type: 'object',
      sortable: this.properties.filter(cur => cur.sortable).map(cur => cur.name),
      filterable: this.properties.filter(cur => cur.filterable).map(cur => cur.name),
      properties: this._subsetProperties('all', 'bson')
    }
  }
}

module.exports = Schema
