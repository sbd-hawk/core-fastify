'use strict'

const Ajv = require('ajv')

// Configure AJV.
const ajv = new Ajv({
  removeAdditional: true,
  useDefaults: true,
  coerceTypes: 'array',
  allErrors: true
})

/**
 * Enum for ways to process a filter.
 * 1. Accept multiples as an array,
 * 2. use first,
 * 3. use last, or
 * 4. process each individually.
 * @readonly
 * @enum {number}
 */
const PROCESS = {
  ARRAY: 1,
  FIRST: 2,
  LAST: 3,
  INDIVIDUALLY: 4
}

/**
 * Model Meta Schema
 */
const modelMetaSchema = {
  type: 'object',
  required: [ 'collectionName', 'schema', 'defaults', 'sortable', 'filters', 'indexes' ],
  additionalProperties: false,
  properties: {
    collectionName: { type: 'string' },
    schema: { type: 'object' },
    defaults: { type: 'object' },
    sortable: { type: 'array', items: { type: 'string' } },
    filters: {
      type: 'object',
      patternProperties: {
        '^.*$': {
          type: 'object',
          required: [ 'process', 'handler' ],
          additionalProperties: false,
          properties: {
            process: { type: 'integer', enum: Object.values(PROCESS) },
            handler: {}
          }
        }
      }
    },
    indexes: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          keys: {
            type: 'object',
            patternProperties: {
              '^.*$': { type: 'integer', enum: [ 1, -1 ] }
            }
          },
          options: {
            type: 'object',
            additionalProperties: false,
            properties: {
              background: { type: 'boolean' },
              unique: { type: 'boolean' },
              name: { type: 'string' },
              sparse: { type: 'boolean' },
              expireAfterSeconds: { type: 'integer' }
            }
          }
        }
      }
    }
  }
}

// Create the model validator.
const validateModel = ajv.compile(modelMetaSchema)

/**
 *
 */
class Model {
  constructor (model) {
    // Validate...
    // Validate the property.
    if (!validateModel(model)) {
      throw new Error(validateModel.errors)
    }

    this._properties = new Set(keyifySchema(model.schema))
    new Set([
      ...model.schema.required,
      ...model.intent.read,
      ...model.intent.write,
      ...model.intent.adminRead,
      ...model.intent.adminWrite,
      ...model.intent.admin,
      ...Object.keys(model.defaults),
      ...model.sortable
    ]).forEach(cur => if (!this._properties.has(cur)) throw new Error(`expected property '${cur}' missing`))

    this._model = model
  }
}
