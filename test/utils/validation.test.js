const BSON = require('bson')
const tap = require('tap')
const ajv = require('../validation')
const { repository } = require('../schemas/abstract')

const defaultData = {
  filterKey: [],
  filterValue: [],
  sortOn: [ '_id' ],
  page: 1,
  perPage: 100
}

tap.test('missing sortable', t => {
  const schema = {
    bsonType: 'object',
    requied: [ 'username', 'password' ],
    filterable: [ 'foo', 'bar' ],
    properties: {
      username: { bsonType: 'string' },
      password: { bsonType: 'string' },
      foo: { bsonType: 'array', items: { bsonType: 'objectId' } },
      bar: { bsonType: 'bool' },
      baz: { bsonType: 'date' },
      other: { bsonType: 'int' }
    }
  }
  const repo = Object.assign({
    repositorySchema: schema,
    additionalProperties: false
  }, repository)
  t.throws(() => ajv.compile(repo))
  t.end()
})

tap.test('missing filterable', t => {
  const schema = {
    bsonType: 'object',
    requied: [ 'username', 'password' ],
    sortable: [ 'username', 'baz' ],
    properties: {
      username: { bsonType: 'string' },
      password: { bsonType: 'string' },
      foo: { bsonType: 'array', items: { bsonType: 'objectId' } },
      bar: { bsonType: 'bool' },
      baz: { bsonType: 'date' },
      other: { bsonType: 'int' }
    }
  }
  const repo = Object.assign({
    repositorySchema: schema,
    additionalProperties: false
  }, repository)
  t.throws(() => ajv.compile(repo))
  t.end()
})

tap.test('missing bsonType in property', t => {
  const schema = {
    bsonType: 'object',
    requied: [ 'username', 'password' ],
    sortable: [ 'username', 'baz' ],
    properties: {
      username: { bsonType: 'string' },
      password: { bsonType: 'string' },
      foo: { bsonType: 'array', items: { bsonType: 'objectId' } },
      bar: { bsonType: 'bool' },
      baz: { bsonType: 'date' },
      other: { bsonType: 'int' },
      final: { type: 'integer' }
    }
  }
  const repo = Object.assign({
    repositorySchema: schema,
    additionalProperties: false
  }, repository)
  t.throws(() => ajv.compile(repo))
  t.end()
})

tap.test('validate repository with default data', t => {
  const schema = {
    bsonType: 'object',
    requied: [ 'username', 'password' ],
    sortable: [ 'username', 'baz' ],
    filterable: [ 'foo', 'bar' ],
    properties: {
      username: { bsonType: 'string' },
      password: { bsonType: 'string' },
      foo: { bsonType: 'array', items: { bsonType: 'objectId' } },
      bar: { bsonType: 'bool' },
      baz: { bsonType: 'date' },
      other: { bsonType: 'int' }
    }
  }
  const repo = Object.assign({
    repositorySchema: schema,
    additionalProperties: false
  }, repository)
  const validate = ajv.compile(repo)
  const valid = validate(defaultData)
  t.ok(valid)
  t.same(defaultData.sort, [ [ '_id', 1 ] ])
  t.same(defaultData.filters, {})
  t.end()
})

tap.test('validate repository', t => {
  const schema = {
    bsonType: 'object',
    requied: [ 'username', 'password' ],
    sortable: [ 'username', 'baz' ],
    filterable: [ 'foo', 'bar' ],
    properties: {
      username: { bsonType: 'string' },
      password: { bsonType: 'string' },
      foo: { bsonType: 'array', items: { bsonType: 'objectId' } },
      bar: { bsonType: 'bool' },
      baz: { bsonType: 'date' },
      other: { bsonType: 'int' }
    }
  }
  const repo = Object.assign({
    repositorySchema: schema,
    additionalProperties: false
  }, repository)
  const validate = ajv.compile(repo)
  const foo1 = BSON.ObjectID()
  const foo2 = BSON.ObjectID()
  const data = {
    filterKey: [ 'foo', 'foo', 'bar' ],
    filterValue: [ foo1.toString(), foo2.toString(), true ],
    sortOn: [ '-baz', 'username' ],
    page: 4,
    perPage: 150
  }
  const valid = validate(data)
  t.ok(valid)
  t.same(data.sort, [ [ 'baz', -1 ], [ 'username', 1 ] ])
  t.same(data.filters, { foo: { $in: [ foo1, foo2 ] }, bar: true })
  t.end()
})
