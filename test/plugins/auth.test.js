'use strict'

const { test } = require('tap')
const Fastify = require('fastify')
const Auth = require('../../plugins/auth')

test('auth works standalone', async (t) => {
  const fastify = Fastify()
  fastify.register(Auth)

  await fastify.ready()
  const password = 'testpassword'
  const hash = await fastify.hashPassword(password)
  t.ok(await fastify.verifyPassword(password, hash))
})
